//
//  GLTableViewCell.h
//  News
//
//  Created by sqx on 15/7/20.
//  Copyright (c) 2015年 蓝欧科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GLTableViewCell : UITableViewCell
@property(nonatomic,retain)UILabel *lable;

@property(nonatomic,retain)UIImageView *imageview;

@end
