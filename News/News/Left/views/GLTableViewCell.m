//
//  GLTableViewCell.m
//  News
//
//  Created by sqx on 15/7/20.
//  Copyright (c) 2015年 蓝欧科技. All rights reserved.
//

#import "GLTableViewCell.h"

@implementation GLTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
    
        self.imageview=[[UIImageView alloc]initWithFrame:CGRectMake(10, 30, 20, 20)];
        [self.contentView addSubview:_imageview];
        self.lable=[[UILabel alloc]initWithFrame:CGRectMake(40, 30, 100, 20)];
        [self.contentView addSubview:_lable];
        self.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
        
    }
    return self;

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
