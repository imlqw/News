//
//  NEWdata.m
//  News
//
//  Created by sqx on 15/7/22.
//  Copyright (c) 2015年 蓝欧科技. All rights reserved.
//

#import "NEWdata.h"

@implementation NEWdata
-(instancetype)init
{
    if (self = [super init]) {
        self.imgArray=[NSMutableArray array];
    }
    return self;
}
-(void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    
}
-(void)setValue:(id)value forKey:(NSString *)key
{
    [super setValue:value forKey:key];
    if ([key isEqualToString:@"imgsrc"]) {
        NSString *str=[NSString stringWithFormat:@"%@",value];
        
        [self.imgArray addObject:str];
    }
    if ([key isEqualToString:@"imgextra"]) {
        for (NSDictionary *dic in value) {
            [self.imgArray addObject:[dic objectForKey:@"imgsrc"]];
        }
    }
    if ([key isEqualToString:@"imgType"]) {
        self.imgtype=[NSString stringWithFormat:@"%@",value];
    }
    if ([key isEqualToString:@"editor"]) {
        self.editorImg=[[value firstObject] objectForKey:@"editorImg"];
        self.editorName=[[value firstObject] objectForKey:@"editorName"];
    }
    if ([key isEqualToString:@"replyCount"]) {
        self.replyCounts=[NSString stringWithFormat:@"%@",value];
    }
}

@end
