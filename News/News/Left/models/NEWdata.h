//
//  NEWdata.h
//  News
//
//  Created by sqx on 15/7/22.
//  Copyright (c) 2015年 蓝欧科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NEWdata : NSObject

@property(nonatomic,retain)NSMutableArray*imgArray;//图片数组
@property(nonatomic,copy)NSString *title;//标题
@property(nonatomic,copy)NSString *digest;//副标题
@property(nonatomic,copy)NSString *docid;//下个页面URL标志
@property(nonatomic,copy)NSString *imgtype;//大图标志 (nsnumber )
@property(nonatomic,copy)NSString *editorImg;//用户图片
@property(nonatomic,copy)NSString *editorName;//用户姓名
@property(nonatomic,copy)NSString *skipType;//下个页面的样式
@property(nonatomic,copy)NSString *skipID;//网址参数
@property(nonatomic,copy)NSString *tname;//头条
@property(nonatomic,copy)NSString *replyCounts;//评论人数
@property(nonatomic,copy)NSString *subtitle;//暂定有用
@property(nonatomic,copy)NSString *url;

@end
