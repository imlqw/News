//
//  DataBaseManager.h
//  News
//
//  Created by sqx on 15/7/21.
//  Copyright (c) 2015年 蓝欧科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DataBaseManager : NSObject
//对于数据库管理类DataBaseManager 只需要具备两个功能
//1.打开数据库
+ (sqlite3 *)openDataBase;
//2.关闭数据
+ (void)closeDataBase;

@end
