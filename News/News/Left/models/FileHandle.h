//
//  FileHandle.h
//  News
//
//  Created by sqx on 15/7/22.
//  Copyright (c) 2015年 蓝欧科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "DataBaseManager.h"
#import"NEWdata.h"
@interface FileHandle : NSObject
@property(nonatomic)BOOL isLogin;//登录状态
+(FileHandle*)shareHandle;

//数据库的存储
//获取数据库路径
-(NSString*)getDataBaseFilePath;

/////插入用户
//-(BOOL)insertUser:(User *)user;
////通过用户名获取密码
//- (NSString *)selectPasswordFromDataBaseWithUserName:(NSString *)userName;

//获取数据
-(NSMutableArray*)selectSaveWithTitle:(NSString*)title;
//收藏
-(void)insertSaveData:(NSArray*)array;
//获取所有数据title
-(NSMutableArray*)selectTitleWithSave;
//删除数据
-(void)deleteDataWithTitle:(NSString *)title;

//缓存数据
-(void)insertNewsData:(NEWdata*)data title:(NSString*)title;
//获取缓存数据
-(NSMutableArray*)selectNewsDataWithTitle:(NSString*)title;
//删除缓存数据
-(void)deleteNewsDataWithTitle:(NSString*)title;


@end
