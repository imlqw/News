//
//  FileHandle.m
//  News
//
//  Created by sqx on 15/7/22.
//  Copyright (c) 2015年 蓝欧科技. All rights reserved.
//

#import "FileHandle.h"

@implementation FileHandle


static FileHandle *filehandle=nil;

+(FileHandle*)shareHandle
{
    @synchronized(self)
    {
        if (filehandle==nil) {
            filehandle=[[FileHandle alloc] init];
            filehandle.isLogin=NO;
        }
    }
    return filehandle;
}


//获取数据库路径
-(NSString*)getDataBaseFilePath
{
    //获取Caches文件路径
    NSString *cachesPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
    //拼接数据库文件路径
    NSString *dbPath = [cachesPath stringByAppendingString:@"/DB.sqlite"];
    return dbPath;
}
//插入用户
//-(BOOL)insertUser:(User *)user
//{
//    NSString *password = nil;
//    //判断用户名是否存在
//    password = [self selectPasswordFromDataBaseWithUserName:user.userName];
//    if (password) {
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"该用户名已存在" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
//        [alertView show];
//        return NO;
//        
//    }
//    
//    //创建可变数组
//    //  NSMutableArray *userNames = [NSMutableArray array];
//    //1.打开数据库
//    sqlite3 *db = [DataBaseManager openDataBase];
//    //2.创建指令集
//    sqlite3_stmt *stmt = nil;
//    
//    NSString *sqlStr = @"insert into User(username,password,surePassword ,phoneNumber ) values(?,?,?,?)";
//    //4.语法检查
//    int flag = sqlite3_prepare_v2(db, [sqlStr UTF8String], -1, &stmt, NULL);
//    //判断语法检查是否正确
//    if (flag == SQLITE_OK) {
//        sqlite3_bind_text(stmt, 1, [user.userName UTF8String], -1, nil);
//        sqlite3_bind_text(stmt, 2, [user.password UTF8String], -1, nil);
//        sqlite3_bind_text(stmt, 3, [user.surePassword UTF8String], -1, nil);
//        sqlite3_bind_text(stmt,4, [user.phoneNumber UTF8String], -1, nil);
//        //执行语句
//        sqlite3_step(stmt);
//    }
//    //8.释放内存
//    sqlite3_finalize(stmt);
//    //关闭数据库
//    [DataBaseManager closeDataBase];
//    return YES;
//    
//}
////通过用户名获取密码
//- (NSString *)selectPasswordFromDataBaseWithUserName:(NSString *)userName{
//    NSString *password = nil;
//    //打开数据库
//    sqlite3 *db = [DataBaseManager openDataBase];
//    //创建指令集
//    sqlite3_stmt *stmt = nil;
//    //sql语句
//    NSString *sqlStr = @"select password from User where username = ?";
//    //语法检查
//    int flag = sqlite3_prepare_v2(db, [sqlStr UTF8String], -1, &stmt, NULL);
//    //判断语法检查
//    if (flag ==SQLITE_OK) {
//        //参数绑定
//        sqlite3_bind_text(stmt, 1, [userName UTF8String], -1, nil);
//        //执行
//        sqlite3_step(stmt);
//        const unsigned char *passWord1 = sqlite3_column_text(stmt, 0);
//        if (passWord1 == NULL) {
//            password = nil;
//        }  else
//        {
//            password = [NSString stringWithUTF8String:(char *)passWord1];
//        }
//        
//    }
//    sqlite3_finalize(stmt);
//    [DataBaseManager closeDataBase];
//    return password;
//}
//获取数据
-(NSMutableArray*)selectSaveWithTitle:(NSString*)title
{
    NSMutableArray*array=[NSMutableArray array];
    //1.打开数据库
    sqlite3 *db=[DataBaseManager openDataBase];
    //2.创建指令集
    sqlite3_stmt *stmt=nil;
    //3.设置SQL语句
    NSString *sqlStr=@"select * from SAVE where title=? ";
    //4.语法检查
    int flag=sqlite3_prepare(db, [sqlStr UTF8String], -1, &stmt, nil);
    //5.判断
    if (flag==SQLITE_OK) {
        //6.绑定                                       长度
        sqlite3_bind_text(stmt, 1, [title UTF8String], -1, nil);
        //执行 布
        sqlite3_step(stmt);
        //获取数据
        for (int i=1; i<5; i++) {
            char *str=(char*)sqlite3_column_text(stmt, i);
            if (str) {
                NSString *StrOC=[NSString stringWithUTF8String:str];
                [array addObject:StrOC];
            }
            
        }
    }
    //8. 释放内存
    sqlite3_finalize(stmt);
    //关闭数据库
    [DataBaseManager closeDataBase];
    return array;
}
//收藏
-(void)insertSaveData:(NSArray*)array
{
    //1.打开数据库
    sqlite3 *db=[DataBaseManager openDataBase];
    //2.创建指令集
    sqlite3_stmt *stmt=nil;
    //3.设置SQL
    NSString *sqlStr=@"insert into SAVE(title,source,ptime,jsstring)values(?,?,?,?)";
    //4.语法检查
    int flag=sqlite3_prepare(db, [sqlStr UTF8String], -1, &stmt, nil);
    //5.panduan
    if (flag==SQLITE_OK) {
        //绑定
        sqlite3_bind_text(stmt, 1, [array[0] UTF8String], -1, nil);
        sqlite3_bind_text(stmt, 2, [array[1] UTF8String], -1, nil);
        sqlite3_bind_text(stmt, 3, [array[2] UTF8String], -1, nil);
        sqlite3_bind_text(stmt, 4, [array[3] UTF8String], -1, nil);
        
        //7.执行
        sqlite3_step(stmt);
    }
    //8.释放所有权
    sqlite3_finalize(stmt);
    //9.关闭数据库
    [DataBaseManager closeDataBase];
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"提示" message:@"收藏成功" delegate:nil cancelButtonTitle:nil otherButtonTitles: nil];
    [self performSelector:@selector(dimissAlert:) withObject:alert afterDelay:1.0];
    [alert show];
    
}
//alert自动消失
- (void) dimissAlert:(UIAlertView *)alert {
    if(alert)     {
        [alert dismissWithClickedButtonIndex:[alert cancelButtonIndex] animated:YES];
    }
}

//获取所有数据title
-(NSMutableArray*)selectTitleWithSave
{
    NSMutableArray *titleArray=[NSMutableArray array];
    //数据库查询
    //1.打开数据库
    sqlite3 *db=[DataBaseManager openDataBase];
    //2.创建指令集
    sqlite3_stmt *stmt=nil;
    //3.设置SQL语句
    NSString *sqlStr=@"select title from SAVE";
    //4.jiancha   准备
    int flag=sqlite3_prepare(db, [sqlStr UTF8String], -1, &stmt, nil);
    //5.
    if (flag==SQLITE_OK) {
        //获取数据
        while (sqlite3_step(stmt)==SQLITE_ROW) {
            if (sqlite3_column_text(stmt, 0)) {
                char *movieName=(char*)sqlite3_column_text(stmt, 0);//查询结果的第几位
                NSString *movieNameOC=[NSString stringWithUTF8String:movieName];
                [titleArray addObject:movieNameOC];
            }
        }
    }
    //释放空间
    sqlite3_finalize(stmt);
    //关闭
    [DataBaseManager closeDataBase];
    return titleArray;
    
}
//删除数据
-(void)deleteDataWithTitle:(NSString *)title
{
    sqlite3 *db=[DataBaseManager openDataBase];
    sqlite3_stmt *stmt=nil;
    NSString *strSQL=@"delete from SAVE where title=?";
    int flag=sqlite3_prepare(db, [strSQL UTF8String], -1, &stmt, nil);
    if (flag==SQLITE_OK) {
        sqlite3_bind_text(stmt, 1, [title UTF8String], -1, nil);
        sqlite3_step(stmt);
    }
    sqlite3_finalize(stmt);
    [DataBaseManager closeDataBase];
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"提示" message:@"已取消收藏" delegate:nil cancelButtonTitle:nil otherButtonTitles: nil];
    [self performSelector:@selector(dimissAlert:) withObject:alert afterDelay:1.0];
    [alert show];
}

//缓存数据
-(void)insertNewsData:(NEWdata*)data title:(NSString*)title
{
    //收藏电影
    //1.打开数据库
    sqlite3 *db=[DataBaseManager openDataBase];
    //2.创建指令集
    sqlite3_stmt *stmt=nil;
    //3.设置SQL
    NSString *sqlStr=@"insert into NewsData(title,newsdata)values(?,?)";
    //4.语法检查
    int flag=sqlite3_prepare(db, [sqlStr UTF8String], -1, &stmt, nil);
    //5.panduan
    if (flag==SQLITE_OK) {
        //绑定
        sqlite3_bind_text(stmt, 1, [title UTF8String], -1, nil);
        //将movie归档
        //1.创建可变data
        NSMutableData *data1=[NSMutableData data];
        //2.创建对象
        NSKeyedArchiver *archiver=[[NSKeyedArchiver alloc] initForWritingWithMutableData:data1];
        //3.归档
        [archiver encodeObject:data forKey:title];
        //4.结束归档
        [archiver finishEncoding];
        //将 data转化为字节 长度
        sqlite3_bind_blob(stmt, 2,data1.bytes, (int)data1.length, nil);
        //7.执行
        sqlite3_step(stmt);
    }
    //8.释放所有权
    sqlite3_finalize(stmt);
    //9.关闭数据库
    [DataBaseManager closeDataBase];
    
}
//获取缓存数据
-(NSMutableArray*)selectNewsDataWithTitle:(NSString*)title{
    NSMutableArray *NewsArray=[NSMutableArray array];
    //数据库查询
    //1.打开数据库
    sqlite3 *db=[DataBaseManager openDataBase];
    //2.创建指令集
    sqlite3_stmt *stmt=nil;
    //3.设置SQL语句
    NSString *sqlStr=@"select newsdata from NewsData where title=?";
    //4.jiancha   准备
    int flag=sqlite3_prepare(db, [sqlStr UTF8String], -1, &stmt, nil);
    //5.
    if (flag==SQLITE_OK) {
        sqlite3_bind_text(stmt, 1, [title UTF8String], -1, nil);
        //获取数据
        while (sqlite3_step(stmt)==SQLITE_ROW) {
            //获取数据
            NSData *data=[NSData dataWithBytes:sqlite3_column_blob(stmt, 0) length:sqlite3_column_bytes(stmt, 0)];
            //反归档
            //创建反归档对象
            NSKeyedUnarchiver *unarchiver=[[NSKeyedUnarchiver alloc] initForReadingWithData:data];
            //反归档操作
            NEWdata *Newsdata=[unarchiver decodeObjectForKey:title];
            //结束反归档
            [unarchiver finishDecoding];
            
            [NewsArray addObject:Newsdata];
        }
    }
    //释放空间
    sqlite3_finalize(stmt);
    //关闭
    [DataBaseManager closeDataBase];
    return NewsArray;
    
}
//删除缓存数据
-(void)deleteNewsDataWithTitle:(NSString*)title
{
    sqlite3 *db=[DataBaseManager openDataBase];
    sqlite3_stmt *stmt=nil;
    NSString *strSQL=@"delete from NewsData where title=?";
    int flag=sqlite3_prepare(db, [strSQL UTF8String], -1, &stmt, nil);
    if (flag==SQLITE_OK) {
        sqlite3_bind_text(stmt, 1, [title UTF8String], -1, nil);
        sqlite3_step(stmt);
    }
    sqlite3_finalize(stmt);
    [DataBaseManager closeDataBase];
}

@end
