//
//  ManagerHandle.m
//  News
//
//  Created by sqx on 15/7/22.
//  Copyright (c) 2015年 蓝欧科技. All rights reserved.
//

#import "ManagerHandle.h"
#import "DataBaseManager.h"
@implementation ManagerHandle
static ManagerHandle *managerHanle=nil;
+(ManagerHandle*)shareHandle
{
    @synchronized(self)
    {
        if (managerHanle==nil) {
            managerHanle=[[ManagerHandle alloc] init];
            //            managerHanle.textFont=1;
            if (![managerHanle getTextFontWithSet]) {
                [managerHanle setSetValue];
            }
          //  managerHanle.nightMode=[managerHanle getNightModeWithSet];
            managerHanle.textFont=[managerHanle getTextFontWithSet];
            
        }
    }
    return managerHanle;
}



//写入初始值
-(void)setSetValue
{
    sqlite3 *db=[DataBaseManager openDataBase];
    sqlite3_stmt *stmt=nil;
    NSString *strSQL=@"INSERT INTO SetTable (nightMode,textFont) VALUES (?,?)";
    int flag=sqlite3_prepare(db, [strSQL UTF8String], -1, &stmt, nil);
    if (flag==SQLITE_OK) {
        sqlite3_bind_int(stmt,1, 0);
        sqlite3_bind_int(stmt,2, 1);
        
        sqlite3_step(stmt);
    }
    sqlite3_finalize(stmt);
    [DataBaseManager closeDataBase];
}

//读取夜间模式属性
-(BOOL)getNightModeWithSet
{
    int i=0;
    sqlite3 *db=[DataBaseManager openDataBase];
    sqlite3_stmt *stmt=nil;
    NSString *strSQL=@"select nightMode from  SetTable";
    int flag=sqlite3_prepare(db, [strSQL UTF8String], -1, &stmt, nil);
    if (flag==SQLITE_OK) {
        while (sqlite3_step(stmt)==SQLITE_ROW) {
            i=sqlite3_column_int(stmt, 0);
        }
        
        
    }
    sqlite3_finalize(stmt);
    [DataBaseManager closeDataBase];
    if (i==0) {
        return NO;
    }else {
        return YES;
    }
}
//读取字体大小
-(NSInteger)getTextFontWithSet
{
    int i=-1;
    sqlite3 *db=[DataBaseManager openDataBase];
    sqlite3_stmt *stmt=nil;
    NSString *strSQL=@"select textFont from  SetTable";
    int flag=sqlite3_prepare(db, [strSQL UTF8String], -1, &stmt, nil);
    if (flag==SQLITE_OK) {
        sqlite3_step(stmt);
        i=sqlite3_column_int(stmt, 0);
        
    }
    sqlite3_finalize(stmt);
    [DataBaseManager closeDataBase];
    return (NSInteger)i;
}
//写入夜间模式
//-(void)setNightModeWithSet
//{
//    sqlite3 *db=[DataBaseManager openDataBase];
//    sqlite3_stmt *stmt=nil;
//    NSString *strSQL=@"update SetTable set nightMode=?  where setID=1";
//    int flag=sqlite3_prepare(db, [strSQL UTF8String], -1, &stmt, nil);
//    if (flag==SQLITE_OK) {
//        if (managerHanle.nightMode) {
//            sqlite3_bind_int(stmt, 1, 1);
//        }else
//        {
//            sqlite3_bind_int(stmt, 1, 0);
//        }
//        sqlite3_step(stmt);
//    }
//    sqlite3_finalize(stmt);
//    [DataBaseManager closeDataBase];
//}
//写入字体大小
-(void)setTextFontWithSet
{
    sqlite3 *db=[DataBaseManager openDataBase];
    sqlite3_stmt *stmt=nil;
    NSString *strSQL=@"update SetTable set textFont=?  where setID=1";
    int flag=sqlite3_prepare(db, [strSQL UTF8String], -1, &stmt, nil);
    if (flag==SQLITE_OK) {
        sqlite3_bind_int(stmt, 1, (int)managerHanle.textFont);
        sqlite3_step(stmt);
    }
    sqlite3_finalize(stmt);
    [DataBaseManager closeDataBase];
}
@end
