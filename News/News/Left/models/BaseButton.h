//
//  BaseButton.h
//  News
//
//  Created by sqx on 15/7/22.
//  Copyright (c) 2015年 蓝欧科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseButton : UIButton
@property(nonatomic,copy)NSString *strURL;//接收网址
@property(nonatomic,copy)NSString *strKey;//储存数据字典的key
@property(nonatomic)BOOL isStatic;//按钮状态
@end
