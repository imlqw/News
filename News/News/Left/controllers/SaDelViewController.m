//
//  SaDelViewController.m
//  News
//
//  Created by sqx on 15/7/22.
//  Copyright (c) 2015年 蓝欧科技. All rights reserved.
//

#import "SaDelViewController.h"
#import"BaseButton.h"
#import"FileHandle.h"
#import"ManagerHandle.h"
#import"UMSocial.h"
#import"UIImageView+WebCache.h"


@interface SaDelViewController ()<UIWebViewDelegate>
{
    CGFloat width;
    CGFloat height;
}
@property(nonatomic,retain)UIWebView *webView;
@property(nonatomic,retain)NSMutableArray *imageArray;
@property(nonatomic,retain)BaseButton *rightBT;
@property(nonatomic,retain)UIView *liteView;
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UILabel*source;//来源
@property(nonatomic,strong)UILabel*ptime;//时间
@property(nonatomic,strong)NSArray *dataArray;//数据；
@property(nonatomic,strong)BaseButton* saveBT;


@end

@implementation SaDelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIBarButtonItem *leftItem=[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemReply target:self action:@selector(dismiss:)];
    self.navigationItem.leftBarButtonItem=leftItem;
    // Uncomment the following line to preserve selection between presentations
    
    self.view.backgroundColor=[UIColor whiteColor];
    width=[UIScreen mainScreen].bounds.size.width;
    height=[UIScreen mainScreen].bounds.size.height;
    self.webView=[[UIWebView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    _webView.backgroundColor=[UIColor whiteColor];
    self.webView.delegate=self;
    //内沿
    self.webView.scrollView.contentInset=UIEdgeInsetsMake(60, 0, 0, 0);
    //禁止超出滑动
    [(UIScrollView *)[[_webView subviews] objectAtIndex:0] setBounces:NO];
    [self.view addSubview:_webView];
    //布局
    [self layoutView];
    
}
-(void)dismiss:(UIBarButtonItem*)bar
{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(void)layoutView
{
    self.titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(10, -60, width, 40)];
    _titleLabel.font=[UIFont systemFontOfSize:20];
    [self.webView.scrollView addSubview:_titleLabel];
    self.source=[[UILabel alloc] initWithFrame:CGRectMake(10, -20, width*0.5, 20)];
    _source.font=[UIFont systemFontOfSize:15];
    [self.webView.scrollView addSubview:_source];
    self.ptime=[[UILabel alloc] initWithFrame:CGRectMake(width*0.36, -20, 150, 20)];
    _ptime.font=[UIFont systemFontOfSize:15];
    [self.webView.scrollView addSubview:_ptime];
    
    
    //收藏
    self.liteView=[[UIView alloc] initWithFrame:self.view.bounds];
    UIView *aview=[[UIView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-100, 0, 100, 61)];
    aview.backgroundColor=[UIColor lightGrayColor];
    [aview.layer setShadowColor:[UIColor grayColor].CGColor];
    [aview.layer setShadowOffset:CGSizeMake(-5, 5)];
    [aview.layer setShadowRadius:2];//阴影的圆角
    [aview.layer setShadowOpacity:1];
    
    //添加手势
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapTouch)];
    [self.webView addGestureRecognizer:tap];
    
    self.saveBT=[BaseButton buttonWithType:UIButtonTypeSystem];
    [_saveBT setTitle:@"取消收藏" forState:UIControlStateNormal];
    _saveBT.isStatic=YES;
    [_saveBT setFrame:CGRectMake(0, 0, 100, 30)];
    [_saveBT addTarget:self action:@selector(saveNews:) forControlEvents:UIControlEventTouchUpInside];
    _saveBT.backgroundColor=[UIColor whiteColor];
    [aview addSubview:_saveBT];
    //分享
    BaseButton* shareBT=[BaseButton buttonWithType:UIButtonTypeSystem];
    [shareBT setTitle:@"分享" forState:UIControlStateNormal];
    [shareBT setFrame:CGRectMake(0, 31, 100, 30)];
    [shareBT addTarget:self action:@selector(shareNews:) forControlEvents:UIControlEventTouchUpInside];
    shareBT.backgroundColor=[UIColor whiteColor];
    [aview addSubview:shareBT];
    [self.liteView addSubview:aview];
    
    //详情页面中的更多
    
    self.rightBT=[BaseButton buttonWithType:UIButtonTypeSystem];
    [_rightBT setTitle:@"更多" forState:UIControlStateNormal];
    _rightBT.frame=CGRectMake(0, 0, 50, 30);
    _rightBT.isStatic=YES;
    [self.rightBT addTarget:self action:@selector(showView:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem=[[UIBarButtonItem alloc] initWithCustomView:_rightBT];
    self.navigationItem.rightBarButtonItem=rightItem;
    
    self.dataArray=[[FileHandle shareHandle] selectSaveWithTitle:self.titleStr];
    self.titleLabel.text=self.dataArray[0];
    self.source.text=self.dataArray[1];
    
    CGRect rect=[_source.text boundingRectWithSize:CGSizeMake(1000, 20) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:_source.font} context:nil];
    self.ptime.frame=CGRectMake(rect.size.width+20, -20, 150, 20);
    self.ptime.text=self.dataArray[2];
    [_webView loadHTMLString:self.dataArray[3] baseURL:nil];
}
//点击事件
-(void)tapTouch
{
    [self.liteView removeFromSuperview];
    _rightBT.isStatic=YES;
}
//收藏
-(void)saveNews:(BaseButton*)button
{
    if (button.isStatic) {
        [[FileHandle shareHandle] deleteDataWithTitle:_titleLabel.text];
        [_saveBT setTitle:@"收藏" forState:UIControlStateNormal];
        //        self.saveIMG.image=[UIImage imageNamed:@"didsave.png"];
        [self.delegate reloadTableView];
        _saveBT.isStatic=NO;
        
    }else{
        [[FileHandle shareHandle] insertSaveData:_dataArray];
        [_saveBT setTitle:@"取消收藏" forState:UIControlStateNormal];
        //        self.saveIMG.image=[UIImage imageNamed:@"save.png"];
        self.saveBT.isStatic=YES;}
}
//分享
-(void)shareNews:(BaseButton*)button
{
    [UMSocialSnsService presentSnsIconSheetView:self
                                         appKey:@"552b2e61fd98c5ec78000649"
                                      shareText:@"你要分享的文字"
                                     shareImage:[UIImage imageNamed:@"icon.png"]
                                shareToSnsNames:[NSArray arrayWithObjects:UMShareToSina,UMShareToTencent,UMShareToRenren,UMShareToFacebook, nil]
                                       delegate:nil];
    //    [UMSocialConfig setTheme:UMSocialThemeBlack];
    
}
//显示更多页面
-(void)showView:(BaseButton*)button
{
    if (button.isStatic) {
        [self.webView addSubview:self.liteView];
        button.isStatic=NO;
    }else{
        [self.liteView removeFromSuperview];
        button.isStatic=YES;
    }
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *fontSize=nil;
    switch ([ManagerHandle shareHandle].textFont) {
        case 0:
            fontSize=@"0.8";
            break;
        case 1:
            fontSize=@"1.0";
            break;
        case 2:
            fontSize=@"1.2";
            break;
            
        default:
            break;
    }
    
    //    [webView stringByEvaluatingJavaScriptFromString:@"document.body.style.fontSize='20px'"];
    [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.body.style.fontSize='%@em'",fontSize]];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
