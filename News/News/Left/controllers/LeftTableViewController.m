//
//  LeftTableViewController.m
//  News
//
//  Created by sqx on 15/7/20.
//  Copyright (c) 2015年 蓝欧科技. All rights reserved.
//

#import "LeftTableViewController.h"
#import"GLTableViewCell.h"
#import"AboutViewController.h"
#import "SETTableViewController.h"
#import"SaveTableViewController.h"
//#import "DKNightVersionManager.h"
@interface LeftTableViewController ()<UIAlertViewDelegate>
@property(nonatomic,retain)UISwitch *swith;
@property(nonatomic,retain)UIButton *button;
@end

@implementation LeftTableViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    if (section==1){
//        return 2;
//    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *gl=@"cell";
    if(indexPath.section==0){
        GLTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:gl];
        if(cell==nil){
            cell=[[GLTableViewCell alloc]initWithStyle:(UITableViewCellStyleValue1) reuseIdentifier:@"cell"];
            
            UIImageView *image=[[UIImageView alloc]initWithFrame:CGRectMake(30, 30, 200, 160)];
            
            image.image=[UIImage imageNamed:@"hh.jpg"];
            
            [cell addSubview:image];
            
            cell.selectionStyle=UITableViewCellSeparatorStyleNone
            ;
        
        }
        return cell;
    }
    else if(indexPath.section==1){
        
        GLTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:gl];
        if(cell==nil){
            cell=[[GLTableViewCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:gl];
            
            cell.lable.text=@"添加收藏";
            cell.imageview.image=[UIImage imageNamed:@"1.png"];
            
        }
        return cell;
        
    
    }else if(indexPath.section==2){
        GLTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:gl];
        if(cell==nil){

        cell=[[GLTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:gl];
        cell.lable.text=@"系统设置";
        cell.imageview.image=[UIImage imageNamed:@"3.png"];
        }
        return cell;
    }else if (indexPath.section==3){
        GLTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:gl];
        if(cell==nil){
            cell=[[GLTableViewCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:gl];
            cell.lable.text=@"夜间模式切换";
            cell.imageview.image=[UIImage imageNamed:@"5.png"];
            
//        _swith=[[UISwitch alloc]initWithFrame:CGRectMake(tableView.bounds.size.width - 115, 25, 30, 50)] ;
//            [_swith setOn:NO];
//            [_swith addTarget:self action:@selector(switchAction:) forControlEvents:        (UIControlEventValueChanged)];
//            [cell addSubview:_swith];
            
            _button=[[UIButton alloc]initWithFrame:CGRectMake(tableView.bounds.size.width - 115, 25, 30, 50)];
            
      
            [_button setTitle:@"dianji " forState:(UIControlStateNormal)];
            
            //_button.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"5.png"]];
            _button.backgroundColor=[UIColor redColor];
            
            [_button addTarget:self action:@selector(changeColor1) forControlEvents:(UIControlEventTouchUpInside)];
            
            [cell addSubview:_button];
            

        }

        return cell;
    }else if (indexPath.section==4){
        GLTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:gl];
        if(cell==nil){
            cell=[[GLTableViewCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:gl];
            cell.lable.text=@"关于我们";
            cell.imageview.image=[UIImage imageNamed:@"4.png"];
        }
        
        return cell;
    
    }
    
    
    
    return nil;
}
//夜间模式切换的方法
//-(void) switchAction:(id)sender{
//    
//    if (_swith.on) {
//        [[UIApplication sharedApplication].delegate window].alpha = 0.3;
//        if ([DKNightVersionManager currentThemeVersion] == DKThemeVersionNight) {
//            [DKNightVersionManager dawnComing];
//        }
//        
//    }else{
//        [[UIApplication sharedApplication].delegate window].alpha = 1;
//        
//        [DKNightVersionManager nightFalling];
//    }
//}

//-(void)changeColor1{
//    
//    if ([DKNightVersionManager currentThemeVersion] == DKThemeVersionNight) {
//      //  [[UIApplication sharedApplication].delegate window].alpha = 0.3;
//        [DKNightVersionManager dawnComing];
//       
//    } else {
//       // [[UIApplication sharedApplication].delegate window].alpha = 1;
//
//        [DKNightVersionManager nightFalling];
//           }
//
//
//}

//点击cell跳转
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //收藏界面
    if(indexPath.section==1){
        SaveTableViewController *savc=[[SaveTableViewController alloc]init];
        UINavigationController *navc=[[UINavigationController alloc]initWithRootViewController:savc];
        navc.navigationBar.translucent=NO;
        navc.hidesBottomBarWhenPushed=YES;
        [self presentViewController:navc animated:YES completion:nil];
    }else if (indexPath.section==2) {
        //设置界面
        SETTableViewController * settable=[[SETTableViewController alloc]init];
        UINavigationController *nanv=[[UINavigationController alloc]initWithRootViewController:settable];
        
             nanv.navigationBar.translucent=NO;
        
             nanv.hidesBottomBarWhenPushed=YES;
        
        [self presentViewController:nanv animated:YES completion:nil];
        
    } else if(indexPath.section==4){
        //关于我们
        AboutViewController *about=[[AboutViewController alloc]init];
        UINavigationController *navc=[[UINavigationController alloc]initWithRootViewController:about];
        
        //关闭毛玻璃效果
        navc.navigationBar.translucent=NO;
        
        //在pop的时候隐藏tablebar
        
        navc.hidesBottomBarWhenPushed=YES;
        
        [self presentViewController:navc animated:YES completion:nil];
        
        
        
    }


}




-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 300;
    }
    return 60;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return 15;
}







/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
