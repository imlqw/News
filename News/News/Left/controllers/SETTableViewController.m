//
//  SETTableViewController.m
//  News
//
//  Created by sqx on 15/7/21.
//  Copyright (c) 2015年 蓝欧科技. All rights reserved.
//

#import "SETTableViewController.h"
#import"ManagerHandle.h"
#import"SetCell.h"
@interface SETTableViewController ()<UIAlertViewDelegate>
@property(nonatomic,copy)NSString *path1;
@property(nonatomic,copy)NSString *path2;//图片文件夹
@property(nonatomic,strong)NSIndexPath *indexNC;
@property(nonatomic,strong)NSIndexPath* index;
@end

@implementation SETTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.view.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"17.jpg"]];
    [self layoutView];
    
    
}
-(void)layoutView{

    UIBarButtonItem *baritem=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemBookmarks target:self action:@selector(dismiss:)];
    
    self.navigationItem.leftBarButtonItem=baritem;
    
    self.navigationItem.title=@"系统设置";
    

}
-(void)dismiss:(UIBarButtonItem *)barbutton{

    [self dismissViewControllerAnimated:YES completion:nil];

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(section==0){
        return 3;
        
    }
    return 2;

   }
    





- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static  NSString *idint=@"123";
    NSArray *array=@[@"大",@"中",@"小"];
    NSArray *cache= @[@"最新版本",@"清除缓存"];
    
    SetCell *cell=[tableView dequeueReusableCellWithIdentifier:idint];
    
    cell.backgroundColor = [UIColor clearColor];//透明

    if (indexPath.section == 0) {
        if (!cell) {
            cell = [[SetCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idint];
            if(indexPath.row==[ManagerHandle shareHandle].textFont){
                self.index=indexPath;
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
            
            
            }
    
            cell.lable.text = array[indexPath.row];
            [cell.lable setFont:[UIFont systemFontOfSize:18]];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
       
        }
        return cell;
    }
    if(indexPath.section==1){
        if(cell==nil){
            cell=[[SetCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idint];
            cell.lable.text=cache[indexPath.row];
            [cell.lable setFont:[UIFont systemFontOfSize:18]];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            if(indexPath.row==0){
                cell.cesslable.text=@"V1.0";
            }if(indexPath.row==1){
                //拼接文件路径
                NSString *path=[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
                self.path1=[path stringByAppendingPathComponent:@"com.hackemist.SDWebImageCache.default"];
                self.path2=[path stringByAppendingPathComponent:@"com.lanou3g.NewsBeta/fsCachedData"];
                
                float size=[self folderSizeAtPath:_path1]+[self folderSizeAtPath:_path2];
                cell.cesslable.text=[NSString stringWithFormat:@"%.1fM",size];
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        return cell;
    
    }
     return  nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section==0){
        SetCell *cell1=(SetCell*)[tableView cellForRowAtIndexPath:self.index];
        cell1.accessoryType=UITableViewCellAccessoryNone;
        
        SetCell *cell=(SetCell*)[tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryType=UITableViewCellAccessoryCheckmark;
        [ManagerHandle shareHandle].textFont=indexPath.row;
        NSLog(@"%ld",[ManagerHandle shareHandle].textFont);
        self.index=indexPath;
    }else if(indexPath.section==1){
        if(indexPath.row==0){
            UIAlertView *alter=[[UIAlertView alloc]initWithTitle:@"提示" message:@"当前已经是最新版本" delegate:self cancelButtonTitle:nil otherButtonTitles:nil];
            [alter show];
            [self performSelector:@selector(dimissAlert:) withObject:alter afterDelay:1.5];
            return;
        } else if (indexPath.row==1) {
            SetCell *cell=(SetCell*)[tableView cellForRowAtIndexPath:indexPath];
            if ([cell.cesslable.text isEqualToString:@"0M"]) {
                [self showAlertView];
            }else{
                self.indexNC=indexPath;
                
                [self showAlertView1];
            }
            return;
        }

    }
}
-(void)showAlertView
{
    UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"提示" message:@"当前缓存为空" delegate:nil cancelButtonTitle:nil otherButtonTitles: nil];
    [alertView show];
    [self performSelector:@selector(dimissAlert:) withObject:alertView afterDelay:1.5];
}
-(void)showAlertView1
{
    UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"提示" message:@"是否清除缓存" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alertView show];
}

- (void) dimissAlert:(UIAlertView *)alert {
    if(alert)     {
        [alert dismissWithClickedButtonIndex:[alert cancelButtonIndex] animated:YES];
    }
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==1) {
        SetCell *cell=(SetCell*)[self.tableView cellForRowAtIndexPath:self.indexNC];
        NSFileManager *fileManage=[NSFileManager defaultManager];
        [fileManage removeItemAtPath:_path1 error:nil];
        [fileManage removeItemAtPath:_path2 error:nil];
        cell.cesslable.text=@"0M";
    }



}

//获取文件夹下的所有文件大小
- (float ) folderSizeAtPath:(NSString*) folderPath
{
    NSFileManager* manager = [NSFileManager defaultManager];
    if (![manager fileExistsAtPath:folderPath]) return 0;
    NSEnumerator *childFilesEnumerator = [[manager subpathsAtPath:folderPath] objectEnumerator];
    NSString* fileName;
    long long folderSize = 0;
    while ((fileName = [childFilesEnumerator nextObject]) != nil){
        NSString* fileAbsolutePath = [folderPath stringByAppendingPathComponent:fileName];
        folderSize += [self fileSizeAtPath:fileAbsolutePath];
    }
    return folderSize/(1024.0*1024.0);
}
//单个文件的大小
- (float) fileSizeAtPath:(NSString*) filePath
{
    NSFileManager* manager = [NSFileManager defaultManager];
    
    if ([manager fileExistsAtPath:filePath]){
        
        return [[manager attributesOfItemAtPath:filePath error:nil] fileSize];
    }
    
    return 0;
}



-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 24;
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSArray *array=@[@"字体设置",@"模式设置",@"版本设置"];
    NSString *arr=array[section];
    return arr;

}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
