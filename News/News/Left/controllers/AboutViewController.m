//
//  AboutViewController.m
//  News
//
//  Created by sqx on 15/7/21.
//  Copyright (c) 2015年 蓝欧科技. All rights reserved.
//

#import "AboutViewController.h"

//这是关于我们的详情页面

@interface AboutViewController ()

@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"12.jpg"]];
        [self layoutView];

}
-(void)layoutView{
    UIBarButtonItem *barbutton=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:(UIBarButtonSystemItemBookmarks) target:self action:@selector(dismiss:)];
    self.navigationItem.leftBarButtonItem=barbutton;
    self.navigationItem.title=@"关于我们";
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake((self.view.bounds.size.width-100 )/2, 220, 100, 30)];
    //    label.backgroundColor = [UIColor cyanColor];
    label.textAlignment=NSTextAlignmentCenter;
    label.text = @"这里有最新的资讯";
    [self.view addSubview: label];
     label.font = [UIFont systemFontOfSize:30];
    label.textColor  = [UIColor redColor];
    label.numberOfLines = 0;
    UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(40, 300, (self.view.bounds.size.width -20), 100)];
        label2.text = @"感谢您的支持";
    label2.font = [UIFont systemFontOfSize:30];
    label2.textColor  = [UIColor redColor];
    label2.numberOfLines = 0;
    [self.view addSubview: label2];
    
    UILabel *label3 = [[UILabel alloc]initWithFrame:CGRectMake(80, 350, (self.view.bounds.size.width -20), 100)];
  
    label3.text = @"我们会做的更好!";
    label3.font = [UIFont systemFontOfSize:30];
    label3.textColor = [UIColor redColor];
    label3.numberOfLines = 0;
    [self.view addSubview: label3];
    

}
-(void)dismiss:(UIBarButtonItem*)bar
{
    [self dismissViewControllerAnimated:YES completion:^{
        nil;
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
