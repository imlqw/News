//
//  SaveTableViewController.m
//  News
//
//  Created by sqx on 15/7/20.
//  Copyright (c) 2015年 蓝欧科技. All rights reserved.
//

#import "SaveTableViewController.h"
#import"SaDelViewController.h"
@interface SaveTableViewController ()<SaDelViewControllerdelage>
@property(nonatomic,strong)NSMutableArray *array;
@end

@implementation SaveTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.array=[NSMutableArray array];
    _array=[[FileHandle shareHandle] selectTitleWithSave];
    
    
    self.tableView.tableFooterView=[[UIView alloc] init];
    UIBarButtonItem *bar=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:(UIBarButtonSystemItemBookmarks) target:self action:@selector(dismiss:)];
    //左边的返回button
    self.navigationItem.leftBarButtonItem=bar;
    
    //右边的编辑button
    self.navigationItem.rightBarButtonItem=self.editButtonItem;
    
    self.navigationItem.title=@"收藏界面";
    
    

}
-(void)dismiss:(UIBarButtonItem*)bar
{
    
    [self dismissViewControllerAnimated:YES completion:^{
        nil;
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {    // Return the number of sections.
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return _array.count;

}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString  *indet=@"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:indet];
    if (!cell) {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:indet];
    }
    cell.textLabel.text=_array[indexPath.row];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SaDelViewController *saveVC=[[SaDelViewController alloc] init];
    saveVC.titleStr=_array[indexPath.row];
    saveVC.delegate=self;
    saveVC.hidesBottomBarWhenPushed = NO;
    [self.navigationController pushViewController:saveVC animated:YES];
}
//代理刷新数据
-(void)reloadTableView
{
    _array=[[FileHandle shareHandle] selectTitleWithSave];
    [self.tableView reloadData];
}
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [[FileHandle shareHandle] deleteDataWithTitle:_array[indexPath.row]];
        [self.array removeObjectAtIndex:indexPath.row];
//        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
