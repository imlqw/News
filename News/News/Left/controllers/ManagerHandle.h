//
//  ManagerHandle.h
//  News
//
//  Created by sqx on 15/7/22.
//  Copyright (c) 2015年 蓝欧科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ManagerHandle : NSObject
@property(nonatomic)BOOL loginState;//登录状态
@property(nonatomic)NSInteger textFont;//字号
//@property(nonatomic)BOOL nightMode;//夜间模式

+(ManagerHandle*)shareHandle;
//读取夜间模式属性
-(BOOL)getNightModeWithSet;
//读取字体大小
-(NSInteger)getTextFontWithSet;
//写入夜间模式
-(void)setNightModeWithSet;
//写入字体大小
-(void)setTextFontWithSet;
@end
