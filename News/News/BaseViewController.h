//
//  BaseViewController.h
//  News
//
//  Created by lanou3g on 15/7/17.
//  Copyright (c) 2015年 蓝欧科技. All rights reserved.
//

#import <UIKit/UIKit.h>

//作为应用中所有视图控制器的基类，实现夜间模式和日间模式切换等功能
@interface BaseViewController : UIViewController

@end
