//
//  MainViewController.m
//  News
//
//  Created by lanou3g on 15/7/17.
//  Copyright (c) 2015年 蓝欧科技. All rights reserved.
//

#import "MainViewController.h"
#import "SearchViewController.h"
#import "LeftTableViewController.h"
#import "MMDrawerController.h"
#import "MMDrawerBarButtonItem.h"
#import "UIViewController+MMDrawerController.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIBarButtonItem *rightBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"搜索" style:UIBarButtonItemStylePlain target:self action:@selector(pushToSearchVC:)];
    
    self.navigationItem.rightBarButtonItem=rightBarButtonItem;
    
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:)];
    leftDrawerButton.title=@"更多";
    self.navigationItem.leftBarButtonItem=leftDrawerButton;
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    self.navigationItem.title=@"VS新闻";
}

-(void)leftDrawerButtonPress:(MMDrawerBarButtonItem *)drawerBarButtonItem{
    
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

-(void)pushToSearchVC:(UIBarButtonItem *)rightBarButtonItem{
    
    SearchViewController *searchVC=[[SearchViewController alloc]init];
    [self.navigationController pushViewController:searchVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
