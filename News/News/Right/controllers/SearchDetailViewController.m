//
//  SearchDetailViewController.m
//  News
//
//  Created by lanou3g on 15/7/20.
//  Copyright (c) 2015年 蓝欧科技. All rights reserved.
//

#import "SearchDetailViewController.h"
#import "RequestData.h"
#import "DetailModel.h"
#import "DetailImageModel.h"

@interface SearchDetailViewController ()<UIWebViewDelegate,RequestFinish>

@property(nonatomic,strong) DetailModel *detailModel;

@property (nonatomic,retain) RequestData *request;

@property (strong, nonatomic)  UIWebView *webView;

@end

@implementation SearchDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    UIBarButtonItem *rightBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"更多" style:UIBarButtonItemStylePlain target:self action:@selector(pushToShareView:)];
    
    self.navigationItem.rightBarButtonItem=rightBarButtonItem;
    
//    用webView展示详情页面的数据
    self.webView=[[UIWebView alloc]initWithFrame:self.view.bounds];
//    设置代理
    self.webView.delegate = self;
//    不要视图回弹
    self.webView.scrollView.bounces=NO;
    [self.view addSubview:self.webView];
    
//    用从前面控制器通过属性传值得到的ID拼接出详情页面的接口
    NSString *url = [NSString stringWithFormat:@"http://c.m.163.com/nc/article/%@/full.html",self.ID];
    
//    请求数据
    self.request=[RequestData shareRequestData];
    
    [self.request requestWithUrl:url delegate:self];
    

}

#pragma mark - 请求数据完成的代理方法 -

-(void)requestSuccess:(id)responseObject{
    
    self.detailModel = [DetailModel detailWithDict:[responseObject objectForKey:self.ID]];
    [self showInWebView];
}

-(void)requestError:(NSError *)error{
    
}

#pragma mark - ******************** 拼接html语言
- (void)showInWebView
{
    NSMutableString *html = [NSMutableString string];
    [html appendString:@"<html>"];
    [html appendString:@"<head>"];
    [html appendFormat:@"<link rel=\"stylesheet\" href=\"%@\">",[[NSBundle mainBundle] URLForResource:@"Details.css" withExtension:nil]];
    [html appendString:@"</head>"];
    
    [html appendString:@"<body>"];
    [html appendString:[self touchBody]];
    [html appendString:@"</body>"];
    
    [html appendString:@"</html>"];
    
    [self.webView loadHTMLString:html baseURL:nil];
}

- (NSString *)touchBody
{
    NSMutableString *body = [NSMutableString string];
    [body appendFormat:@"<div class=\"title\">%@</div>",self.detailModel.title];
    [body appendFormat:@"<div class=\"time\">%@</div>",self.detailModel.ptime];
    if (self.detailModel.body != nil) {
        [body appendString:self.detailModel.body];
    }
    // 遍历img
    for (DetailImageModel *detailImgModel in self.detailModel.img) {
        NSMutableString *imgHtml = [NSMutableString string];
        
        // 设置img的div
        [imgHtml appendString:@"<div class=\"img-parent\">"];
        
        // 数组存放被切割的像素
        NSArray *pixel = [detailImgModel.pixel componentsSeparatedByString:@"*"];
        CGFloat width = [[pixel firstObject]floatValue];
        CGFloat height = [[pixel lastObject]floatValue];
        // 判断是否超过最大宽度
        CGFloat maxWidth = [UIScreen mainScreen].bounds.size.width * 0.96;
        if (width > maxWidth) {
            height = maxWidth / width * height;
            width = maxWidth;
        }
        
        NSString *onload = @"this.onclick = function() {"
        "  window.location.href = 'sx:src=' +this.src;"
        "};";
        [imgHtml appendFormat:@"<img onload=\"%@\" width=\"%f\" height=\"%f\" src=\"%@\">",onload,width,height,detailImgModel.src];
        // 结束标记
        [imgHtml appendString:@"</div>"];
        // 替换标记
        [body replaceOccurrencesOfString:detailImgModel.ref withString:imgHtml options:NSCaseInsensitiveSearch range:NSMakeRange(0, body.length)];
    }
    return body;
}




-(void)pushToShareView:(UIBarButtonItem *)barButtonItem{
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
