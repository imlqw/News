//
//  SearchDetailViewController.h
//  News
//
//  Created by lanou3g on 15/7/20.
//  Copyright (c) 2015年 蓝欧科技. All rights reserved.
//

#import "BaseViewController.h"
@class NewsModel;

//点击搜索得到的cell进入的详情页面的控制器
@interface SearchDetailViewController : BaseViewController

@property(nonatomic,strong) NewsModel *newsModel;

@property (nonatomic,assign) NSInteger index;

//用来接收从SearchViewController传来的ID,这个ID需要拼接到URL中作为详情数据接口；属性传值
@property (nonatomic,retain) NSString *ID;

@end
