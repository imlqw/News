//
//  SearchBackOnlyTextCell.m
//  News
//
//  Created by lanou3g on 15/7/21.
//  Copyright (c) 2015年 蓝欧科技. All rights reserved.
//

#import "SearchBackOnlyTextCell.h"
#import "SearchBackModel.h"

@implementation SearchBackOnlyTextCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.titleLabel=[[UILabel alloc]init];
        self.titleLabel.numberOfLines=0;
        self.titleLabel.font=[UIFont systemFontOfSize:16];
        [self addSubview:self.titleLabel];
        
        self.timeLabel=[[UILabel alloc]init];
        self.timeLabel.font=[UIFont systemFontOfSize:12];
        [self addSubview:self.timeLabel];
        
        
    }
    return self;
}

-(void)setSearchBackModel:(SearchBackModel *)searchBackModel{
    
//    下面四行代码用来处理从网络请求来的ptime
    NSString *str=[NSString stringWithFormat:@"%@",searchBackModel.ptime];
    
    NSString *string=[str substringToIndex:10];
    
    double number=[string doubleValue];
    
    NSDate *time = [NSDate dateWithTimeIntervalSince1970:number+8*60*60];
    
    self.timeLabel.text=[[NSString stringWithFormat:@"%@",time] substringToIndex:10];
    self.titleLabel.text=[SearchBackModel filterHTML:searchBackModel.title];
    
    self.titleLabel.frame=CGRectMake(0, 10, 355, [SearchBackModel stringHeight:searchBackModel.title]);
    self.timeLabel.frame=CGRectMake(280, 10+5+[SearchBackModel stringHeight:searchBackModel.title], 80, 10);
}



- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
