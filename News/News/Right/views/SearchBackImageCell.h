//
//  SearchBackImageCell.h
//  News
//
//  Created by lanou3g on 15/7/21.
//  Copyright (c) 2015年 蓝欧科技. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SearchBackModel;

//搜索之后得到的有图片的数据用这个cell展示
@interface SearchBackImageCell : UITableViewCell

@property (nonatomic,retain) UILabel *titleLabel;

@property (nonatomic,retain) UILabel *timeLabel;

@property (nonatomic,retain) UIImageView *leftImageView;

//声明model对象，主要是给视图赋值；
@property (nonatomic,strong) SearchBackModel *searchBackModel;

@end
