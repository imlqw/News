//
//  HotWordsCollectionViewCell.m
//  News
//
//  Created by lanou3g on 15/7/18.
//  Copyright (c) 2015年 蓝欧科技. All rights reserved.
//

#import "HotWordsCollectionViewCell.h"

@implementation HotWordsCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        self.hotWordsLabel=[[UILabel alloc]init];
        self.hotWordsLabel.frame=self.contentView.bounds;
        [self.contentView addSubview:_hotWordsLabel];
    }
    return self;
}

@end
