//
//  HotWordsCollectionViewCell.h
//  News
//
//  Created by lanou3g on 15/7/18.
//  Copyright (c) 2015年 蓝欧科技. All rights reserved.
//

#import <UIKit/UIKit.h>

//近期热词(CollectionView)的cell
@interface HotWordsCollectionViewCell : UICollectionViewCell

@property (nonatomic,retain) UILabel *hotWordsLabel;

@end
