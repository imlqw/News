//
//  SearchBackImageCell.m
//  News
//
//  Created by lanou3g on 15/7/21.
//  Copyright (c) 2015年 蓝欧科技. All rights reserved.
//

#import "SearchBackImageCell.h"
#import "SearchBackModel.h"

@implementation SearchBackImageCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.titleLabel=[[UILabel alloc]init];
        self.titleLabel.numberOfLines=0;
        self.titleLabel.font=[UIFont systemFontOfSize:16];
        [self addSubview:self.titleLabel];
        
        self.timeLabel=[[UILabel alloc]init];
        self.timeLabel.font=[UIFont systemFontOfSize:12];
        [self addSubview:self.timeLabel];
        
        self.leftImageView=[[UIImageView alloc]init];
        [self addSubview:self.leftImageView];
    }
    return self;
}

-(void)setSearchBackModel:(SearchBackModel *)searchBackModel{
    
    //    下面四行代码用来处理从网络请求来的ptime，分析之后这样写
    NSString *str=[NSString stringWithFormat:@"%@",searchBackModel.ptime];
    
    NSString *string=[str substringToIndex:10];
    
    double number=[string doubleValue];
    
    NSDate *time = [NSDate dateWithTimeIntervalSince1970:number+8*60*60];
    
    self.timeLabel.text=[[NSString stringWithFormat:@"%@",time] substringToIndex:10];
//    用类方法去掉html的标签
    self.titleLabel.text=[SearchBackModel filterHTML:searchBackModel.title];
    
    self.titleLabel.frame=CGRectMake(95, 10, 260, [SearchBackModel stringHeight:searchBackModel.title]);
    self.timeLabel.frame=CGRectMake(280, 10+5+[SearchBackModel stringHeight:searchBackModel.title], 80, 10);
    self.leftImageView.frame=CGRectMake(0, 10, 80, [SearchBackModel stringHeight:searchBackModel.title]+10+5);

    
    [searchBackModel addObserver:self forKeyPath:@"image" options:NSKeyValueObservingOptionNew context:nil];
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    
    self.leftImageView.image=[change objectForKey:@"new"];
    
    [object removeObserver:self forKeyPath:@"image"];
}


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
