//
//  HotWordsModel.h
//  News
//
//  Created by lanou3g on 15/7/18.
//  Copyright (c) 2015年 蓝欧科技. All rights reserved.
//

#import <Foundation/Foundation.h>

//这个类作为“近期热词”的数据模型
@interface HotWordsModel : NSObject


@property (nonatomic,copy) NSString *hotWord;

@property (nonatomic,copy) NSString *title;

@end
