//
//  SearchBackModel.h
//  News
//
//  Created by lanou3g on 15/7/18.
//  Copyright (c) 2015年 蓝欧科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

//这个类作为搜索得到的数据的model类
@interface SearchBackModel : NSObject

//新闻标题
@property (nonatomic,retain) NSString *title;
//id,用来拼接到URL中作为详情数据接口
@property (nonatomic,retain) NSString *ID;
//新闻的时间
@property (nonatomic,retain) NSString *ptime;
//图片的获取接口
@property (nonatomic,retain) NSString *imageUrlString;
//用来给cell的图片赋值
@property (nonatomic,retain) UIImage *image;
//这个属性用来下载图片，SDWebImage的方法要用
@property (nonatomic,retain) UIImageView *imageView;

//声明了一个计算字符串高度的类方法；
+ (CGFloat)stringHeight:(NSString *)str;
//ios去掉字符串中的html标签的方法
+(NSString *)filterHTML:(NSString *)html;
//下载图片的方法
-(void)startDownImage;

@end
