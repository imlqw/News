//
//  SearchBackModel.m
//  News
//
//  Created by lanou3g on 15/7/18.
//  Copyright (c) 2015年 蓝欧科技. All rights reserved.
//

#import "SearchBackModel.h"
#import "UIImageView+WebCache.h"

@implementation SearchBackModel

//定义了一个计算字符串高度的方法
+ (CGFloat)stringHeight:(NSString *)str {
    //    设置字符串的属性
    NSDictionary *dic = @{NSFontAttributeName : [UIFont systemFontOfSize:17]};
    //    计算字符串的区域
    CGRect rect = [str boundingRectWithSize:CGSizeMake(300, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil];
    return rect.size.height;
}

//ios去掉字符串中的html标签的方法
+(NSString *)filterHTML:(NSString *)html
{
    NSScanner * scanner = [NSScanner scannerWithString:html];
    NSString * text = nil;
    while([scanner isAtEnd]==NO)
    {
        //找到标签的起始位置
        [scanner scanUpToString:@"<" intoString:nil];
        //找到标签的结束位置
        [scanner scanUpToString:@">" intoString:&text];
        //替换字符
        html = [html stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>",text] withString:@""];
    }
    //    NSString * regEx = @"<([^>]*)>";
    //    html = [html stringByReplacingOccurrencesOfString:regEx withString:@""];
    return html;
}

-(void)startDownImage{
    
    self.imageView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"picholder"]];
//    SDWebImage里面下载图片的方法
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:_imageUrlString] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        self.image=image;
    }];
}

@end
