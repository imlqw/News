//
//  RequestData.m
//  Activity
//
//  Created by 左建军 on 15/6/30.
//  Copyright (c) 2015年 蓝鸥科技. All rights reserved.
//

#import "RequestData.h"
#import "AFNetworking.h"

static RequestData *requestData=nil;

@implementation RequestData

//实现请求数据的单例方法
+(RequestData *)shareRequestData{
    
    @synchronized(self){
        if (nil==requestData) {
            requestData=[[RequestData alloc]init];
        }
        
        return requestData;
    }
}

- (void)requestWithUrl:(NSString *)url delegate:(id<RequestFinish>)delegate{
    
//    在这个方法里设置代理，省得忘记
    self.delegate=delegate;
    
//    网络请求都要创建管理器
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    // 网络访问是异步的,回调是主线程的,因此程序员不用管在主线程更新UI的事情
    [manager GET:url   parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [_delegate requestSuccess:responseObject];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [_delegate requestError:error];
        
        NSLog(@"%@",error);
    }];
    
    
    
    
}



@end
