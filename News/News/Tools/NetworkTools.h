//
//  NetworkTools.h
//  News
//
//  Created by lanou3g on 15/7/22.
//  Copyright (c) 2015年 蓝欧科技. All rights reserved.
//

#import "AFHTTPSessionManager.h"

@interface NetworkTools : AFHTTPSessionManager

+ (instancetype)sharedNetworkTools;

@end
