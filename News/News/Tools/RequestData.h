//
//  RequestData.h
//  Activity
//
//  Created by 左建军 on 15/6/30.
//  Copyright (c) 2015年 蓝鸥科技. All rights reserved.
//

#import <Foundation/Foundation.h>

//将网络请求封装在这个类里面，方便操作

@protocol RequestFinish <NSObject>

- (void)requestSuccess:(id)responseObject;

- (void)requestError:(NSError *)error;

@end


@interface RequestData : NSObject

@property (nonatomic, assign)id<RequestFinish> delegate;


- (void)requestWithUrl:(NSString *)url delegate:(id<RequestFinish>)delegate;

//创建请求数据的单例方法，方便操作
+(RequestData *)shareRequestData;

@end
