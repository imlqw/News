//
//  AppDelegate.m
//  News
//
//  Created by lanou3g on 15/7/15.
//  Copyright (c) 2015年 蓝欧科技. All rights reserved.
//

#import "AppDelegate.h"
#import "MainViewController.h"
#import "MMDrawerController.h"//导入第三方抽屉控制器，作为整个应用的大（主）控制器
#import "LeftTableViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
//    创建新闻主界面的主控制器
    MainViewController *mainVC=[[MainViewController alloc]init];
//    创建抽屉控制器的左边控制器
    LeftTableViewController *leftVC=[[LeftTableViewController alloc]init];
//    创建导航控制器，把mainVC作为它的根控制器
    UINavigationController *NVC=[[UINavigationController alloc]initWithRootViewController:mainVC];
//    创建抽屉控制器，把导航控制器作为它的中间控制器，leftVC作为左边控制器
    MMDrawerController *drawerController=[[MMDrawerController alloc]initWithCenterViewController:NVC leftDrawerViewController:leftVC];
    
    [drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    
    self.window.rootViewController=drawerController;
    
//    设置所有导航条的颜色为红色
    [[UINavigationBar appearance] setBackgroundColor:[UIColor redColor]];
    
    
    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
