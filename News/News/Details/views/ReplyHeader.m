//
//  ReplyHeader.m
//  News
//
//  Created by lanou3g on 15/7/23.
//  Copyright (c) 2015年 蓝欧科技. All rights reserved.
//

#import "ReplyHeader.h"

@implementation ReplyHeader

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

// 类方法快速返回热门跟帖的view 
+ (instancetype)replyViewFirst
{
    NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"ReplyHeader" owner:nil options:nil];
    return [array firstObject];
}

/* 类方法快速返回最新跟帖的view */
+ (instancetype)replyViewLast
{
    NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"ReplyHeader" owner:nil options:nil];
    return [array lastObject];
}

@end
