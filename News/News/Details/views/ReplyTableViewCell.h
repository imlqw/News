//
//  ReplyTableViewCell.h
//  News
//
//  Created by lanou3g on 15/7/23.
//  Copyright (c) 2015年 蓝欧科技. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ReplyModel;

@interface ReplyTableViewCell : UITableViewCell

@property(nonatomic,strong) ReplyModel *replyModel;
/** 用户的发言 */
@property (weak, nonatomic) IBOutlet UILabel *sayLabel;

@end
