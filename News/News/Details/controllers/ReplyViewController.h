//
//  ReplyViewController.h
//  News
//
//  Created by lanou3g on 15/7/23.
//  Copyright (c) 2015年 蓝欧科技. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ReplyModel;

@interface ReplyViewController : UIViewController

@property(nonatomic,strong) NSMutableArray *replys;

@end
