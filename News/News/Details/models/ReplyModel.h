//
//  ReplyModel.h
//  News
//
//  Created by lanou3g on 15/7/23.
//  Copyright (c) 2015年 蓝欧科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReplyModel : NSObject

/** 用户的姓名 */
@property(nonatomic,copy) NSString *name;
/** 用户的ip信息 */
@property(nonatomic,copy) NSString *address;
/** 用户的发言 */
@property(nonatomic,copy) NSString *say;
/** 用户的点赞 */
@property(nonatomic,copy) NSString *suppose;

@end
